OPA Controller
==============

This repository contains the firmware for the unit which controls the motion devices in an optical parametric amplifier (OPA).
The motion devices can be linear or rotational stages, used to adjust optical delays or crystal angles, from the Thorlabs Elliptec series of piezo stages.

The heart of the controller is a ATMega 32u4 "Arduino compatible" microcontroller, often referred to as a "Pro Micro". Communication to the stages is via a standard serial connection, however multiple devices are connected to the same serial bus in a "multi-drop" arrangement which is in contrast to a standard RS-232 point-to-point style configuration, and does complicate communications somewhat.

The human interface to the controller is via a 16x2 character LCD and a rotary encoder control. A USB interface to a PC is available but is only required for firmware updates on the controller. In future, the USB connection could be used to provide more sophisticated PC control of the OPA.

Power is provided through a 5.5/2.1 mm barrel jack, at 5 V and approximately 1 A per connected stage.


Using the Controller
--------------------

- Upon power up, the controller will wait for all stages to initialise, perform a homing operation, then move each stage to its saved position.

- A stage name and position is displayed on the LCD.
  Turn the rotary encoder to adjust the position of that stage.
  A stage may also be moved manually, and the display will update with the currently detected stage position.

- Press the rotary encoder to switch control to the next stage.

- Hold the rotary encoder down to save the current position of the selected stage.
  The position will be used as the initial position upon next power up.


Microcontroller Connections
---------------------------

Pins used on the Pro Micro, assuming two stages connected via a 16-wire ribbon cable:

```
5V   RAW        Raw 5V input (ribbon 1, 9)
1    TXO        RX (ribbon 3, 11) via voltage divider
0    RXI        TX (ribbon 2, 10) with 3V pullup
2    I2C_SDA    Display
3    I2C_SCL    Display
4    A6         -
5               -
6    A7         -
7               Rotary encoder quadrature phase A
8    A8         Rotary encoder quadrature phase B
9    A9         Rotary encoder button switch
10   A10        -
16   SPI_MOSI   -
14   SPI_MISO   -
15   SPI_SCLK   -
18   A0         -
19   A1         -
20   A2         -
21   A3         -
0V   GND        Ground (ribbon 8, 16)
```

Stage Configuration
-------------------

The connected stages are defined in the `main.cpp` code in the `stages[]` array of `stage` structs.

The `stage` struct is defined as:

```
struct stage {
  char name[17];          // * Friendly name of stage to display (16 chars max)
  int32_t position;       //   Current position (in encoder counts)
  int32_t position_min;   // * Minimum allowed position (in encoder counts)
  int32_t position_max;   // * Maximum allowed position (in encoder counts)
  bool position_wrap;     // * Wrap around position when min/max exceeded? (for rotation stages)
  bool move_required;     //   Need to send a movement command?
  int32_t stepsize;       // * Increment for movements, negative OK if encoder seems backwards
};
```

The fields marked with `*` should be set to configure the behaviour of the stage.
The remaining fields are used internally by the software.

Each connected stage should be defined in the `stages[]` array.
For example, for a rotation and translation stage, the definition may look like:

```
stage stages[] = {
  {
    "Crystal 1", // name (16 chars max)
    0,           // position
    -143360,     // position_min
    143360,      // position_max
    true,        // position_wrap
    false,       // move_required
    4,           // stepsize
  },
  {
    "Delay 1",   // name (16 chars max)
    0,           // position
    0,           // position_min
    61440,       // position_max
    false,       // position_wrap
    false,       // move_required
    64,          // stepsize
  },
};
```


Development
-----------

The software was developed using the PlatformIO tools using the Arduino Leonardo framework and board type.
