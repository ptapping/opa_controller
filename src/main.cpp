/*
OPA Controller

Copyright 2022 Patrick Tapping

This program is free software: you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not,
see <https://www.gnu.org/licenses/>. 
*/



/*
Pins used on Pro Micro (Arduino Leonardo compatible):

5V  RAW       Raw 5V input (ribbon 1, 9)
1   TXO       RX (ribbon 3, 11) via voltage divider
0   RXI       TX (ribbon 2, 10) with 3V pullup
2   I2C_SDA   Display
3   I2C_SCL   Display
4   A6        -
5             -
6   A7        -
7             Rotary encoder quadrature DT
8   A8        Rotary encoder quadrature CLK
9   A9        Rotary encoder button SW
10  A10       -
16  SPI_MOSI  -
14  SPI_MISO  -
15  SPI_SCLK  -
18  A0        -
19  A1        -
20  A2        -
21  A3        -
0V  GND       Ground (ribbon 8, 16)
*/


#include <Arduino.h>
#include <EEPROM.h>

#include <IoAbstraction.h>
#include <TaskManagerIO.h>
#include <LiquidCrystal_I2C.h>

// Structure containing stage information and status
struct stage {
  char name[17];          // Friendly name of stage
  int32_t position;       // Current position
  int32_t position_min;   // Minimum allowed position
  int32_t position_max;   // Maximum allowed position
  bool position_wrap;     // Wrap around position when min/max exceeded?
  bool move_required;     // Need to send a movement command?
  int32_t stepsize;       // Increment when changing position, negative OK if encoder seems backwards
};

////////////////////////////////////////////////////////////////////////////////
// Define the stages to control here, maximum number allowed is 16.
// ELL20 has 1024 counts/mm, ELL14 or ELL18 have 143360 counts/rev
////////////////////////////////////////////////////////////////////////////////
/* For example:
stage stages[] = {
  {
    "Crystal 1", // name (16 chars max)
    3456,        // position
    -143360,     // position_min
    143360,      // position_max
    true,        // position_wrap
    false,       // move_required
    -4,          // stepsize
  },
  {
    "Delay 1",   // name (16 chars max)
    10240,       // position
    0,           // position_min
    61440,       // position_max
    false,       // position_wrap
    false,       // move_required
    -64,         // stepsize
  },
};
*/
stage stages[] = {
  {
    "Crystal 1", // name (16 chars max)
    3456,        // position
    -143360,     // position_min
    143360,      // position_max
    true,        // position_wrap
    false,       // move_required
    4,           // stepsize
  },
  {
    "Crystal 2", // name (16 chars max)
    4567,        // position
    -143360,     // position_min
    143360,      // position_max
    true,        // position_wrap
    false,       // move_required
    4,           // stepsize
  },
};
// Number of stages defined in the above array
const size_t stage_count = (sizeof(stages)/sizeof(stage));

////////////////////////////////////////////////////////////////////////////////
// Customise loop, display timings if desired
////////////////////////////////////////////////////////////////////////////////
// Delay between update loop iterations.
const unsigned long DELAY_LOOP = 100;
// Delay between position update queries.
const unsigned long DELAY_UPDATE = 1500;
// Delay after displaying a message on the LCD.
const unsigned long DELAY_MSG = 1000;
// Delay to use while homing a stage
const unsigned long DELAY_HOMING = 2000;

////////////////////////////////////////////////////////////////////////////////
// Hardware configuration, change for different hardware constructions
////////////////////////////////////////////////////////////////////////////////
// The rotary encoder has input pins for quadrature A and B (or "clock" and "direction") plus a press button.
// Pin A must be able to trigger a hardware interrupt.
// The Pro Micro interrupt enabled pins are 3 (INT0, I2C SCL), 2 (INT1, I2C SDA), 0 (INT2, Serial1 RXI), 1 (INT3 Serial1 TXO), 7 (INT6)
const uint8_t encoderAPin = 7;  // 7 is the only unused interrupt enabled pin
const uint8_t encoderBPin = 8;
const uint8_t encoderPressPin = 9;

// Status codes returned by get_status()
const uint8_t STATUS_OK = 0;
const uint8_t STATUS_ERROR_COMMS = 128;
const uint8_t STATUS_ERROR_PARSING = 129;
const uint8_t STATUS_ERROR_STAGE_INDEX = 130;

// Position value to indicate invalid/error condition
const int32_t POSITION_ERROR = -2147483648;

// Keep track of time in millis() when last move command was sent (don't spam serial lines)
unsigned long last_update_time;

// Last known value from the rotary encoder callback
int encoder_prev = 0;

// Index of stage currently being controlled
uint8_t stage_current = 0;

// A temporary buffer for building lines of text for comms or display
char linebuffer[64];

// Set up the I2C LCD, I2C address 0x27, 16 columns, 2 rows 
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Limit the value of a stage position, either by wrapping or clipping
void apply_position_limits(uint8_t stage_i) {
  if (stages[stage_i].position_wrap) {
    // Wrap position values
    while (stages[stage_i].position < stages[stage_i].position_min) {
      stages[stage_i].position += stages[stage_i].position_max - stages[stage_i].position_min;
    }
    while (stages[stage_i].position >= stages[stage_i].position_max) {
      stages[stage_i].position -= stages[stage_i].position_max - stages[stage_i].position_min;
    }
  } else {
    // Clip position values
    if (stages[stage_i].position < stages[stage_i].position_min) {
      stages[stage_i].position = stages[stage_i].position_min;
    } else if (stages[stage_i].position > stages[stage_i].position_max) {
      stages[stage_i].position = stages[stage_i].position_max;
    }
  }
}


// Update the display with currently controlled stage position
void display_stage_position(uint8_t stage_i) {
  snprintf(linebuffer, sizeof(linebuffer), "%-16s", stages[stage_i].name);
  lcd.setCursor(0, 0);
  lcd.println(linebuffer);
  snprintf(linebuffer, sizeof(linebuffer), "%16ld", stages[stage_i].position);
  lcd.setCursor(0, 1);
  lcd.print(linebuffer);
}


// Reset communications with the serial devices.
// Send a newline to clear the device's read buffer,
// the read and discard anything on the Serial1 input buffer.
void clear_buffers() {
  Serial1.println();
  while (Serial1.available()) {
    Serial1.read();
  }
}


// Handle rotary encoder button press
void onSpinwheelClicked(pinid_t pin, bool heldDown) {
  if (heldDown) {
    // Button held down:
    // Save position of current stage
    // Note that a press would have already been registered,
    // so it's actually the previous one we want to save
    uint8_t stage_i = (stage_current - 1) % stage_count;
    snprintf(linebuffer, sizeof(linebuffer), "%-16s", stages[stage_i].name);
    lcd.setCursor(0, 0);
    lcd.print(linebuffer);
    lcd.setCursor(0, 1);
    lcd.print("Default saved.  ");
    // Save the default to EEPROM
    EEPROM.put(stage_i*sizeof(stages[stage_i].position), stages[stage_i].position);
    // Keep message on screen for a bit before next update
    last_update_time = millis();
  } else {
    // Button clicked:
    // Change currently controlled stage
    stage_current = (stage_current + 1) % stage_count;
    display_stage_position(stage_current);
  }
}


// Handle rotation of the rotary encoder
void onEncoderChange(int value) {
  // Update current stage position, flag that a move request is needed
  stages[stage_current].position += (value - encoder_prev) * stages[stage_current].stepsize;
  apply_position_limits(stage_current);
  stages[stage_current].move_required = true;
  encoder_prev = value;
}


// Get the status of a stage.
// A return value == 0 == STATUS_OK, 0 < value < 256 is error code.
// A value > 128 indicates an error communicating or parsing the returned string.
uint8_t get_status(uint8_t stage_i) {
  // Ensure we start with clear comms channels
  clear_buffers();
  // Send status request
  snprintf(linebuffer, sizeof(linebuffer), "%1Xgs", stage_i);
  Serial1.print(linebuffer);
  // Attempt to read the response
  memset(linebuffer, 0, sizeof(linebuffer));
  size_t n_bytes = Serial1.readBytesUntil('\n', linebuffer, sizeof(linebuffer));
  if (n_bytes >= 5) {
    // Attempt to parse status code from response
    uint8_t response_stage_i = 255;
    uint8_t response_value = STATUS_ERROR_COMMS;
    if (sscanf(linebuffer, "%1hhxGS%hhx", &response_stage_i, &response_value) == 2) {
      // Response seems OK, check stage_i matches that of request
      if (response_stage_i != stage_i) {
        return STATUS_ERROR_STAGE_INDEX;
      }
      return response_value;
    } else {
      // Couldn't parse response
      return STATUS_ERROR_PARSING;
    }
  }
  // Not enough bytes in the returned string
  return STATUS_ERROR_COMMS;
}


// Helper for home, set_position
// Read either a GS or PO response
uint8_t read_status_response(uint8_t stage_i) {
  // Valid responses are either status (GS) or position (PO)
  memset(linebuffer, 0, sizeof(linebuffer));
  size_t n_bytes = Serial1.readBytesUntil('\n', linebuffer, sizeof(linebuffer));
  if (n_bytes >= 5) {
    // Attempt to parse response
    uint8_t response_stage_i = 255;
    char response_type[] = "XX";
    uint32_t response_value = STATUS_ERROR_COMMS;
    if (sscanf(linebuffer, "%1hhx%2c%lx", &response_stage_i, response_type, &response_value) == 3) {
      // Response seems OK, check stage_i matches that of request
      if (response_stage_i != stage_i) {
        return STATUS_ERROR_STAGE_INDEX;
      }
      // Check response type (status or position)
      if (strncmp(response_type, "PO", 2) == 0) {
        // Position response
        return STATUS_OK;
      } else if (strncmp(response_type, "GS", 2) == 0) {
        // Status response
        return (uint8_t)response_value;
      }
      return response_value;
    } else {
      // Couldn't parse response
      return STATUS_ERROR_PARSING;
    }
  }
  // Not enough bytes in the returned string
  return STATUS_ERROR_COMMS;
}


// Request a stage homing operation.
// Return values are the same as those for get_status()
uint8_t home(uint8_t stage_i) {
  // Ensure we start with clear comms channels
  clear_buffers();
  // Send home request
  snprintf(linebuffer, sizeof(linebuffer), "%1Xho0", stage_i);
  Serial1.print(linebuffer);
  // Valid responses are either status (GS) or position (PO)
  return read_status_response(stage_i);
}


// Get the current stage position
int32_t get_position(uint8_t stage_i) {
  // Ensure we start with clear comms channels
  clear_buffers();
  // Send position query
  snprintf(linebuffer, sizeof(linebuffer), "%1Xgp", stage_i);
  Serial1.print(linebuffer);
  // Attempt to read the response
  memset(linebuffer, 0, sizeof(linebuffer));
  if (Serial1.readBytesUntil('\n', linebuffer, sizeof(linebuffer)) > 4) {
    // Attempt to parse the position, should be in form "0POX\r\n" or "1poXXXXXXXX\r\n" etc
    uint8_t response_stage_i = 255;
    int32_t response_position = POSITION_ERROR;
    if (sscanf(linebuffer, "%1hhxPO%lx", &response_stage_i, (uint32_t*)(&response_position)) == 2) {
      // Response seems OK, check stage_i matches that of request
      if (response_stage_i == stage_i) {
        return response_position;
      }
    }
  }
  return POSITION_ERROR;
}


// Request a move to a given position.
// Return values are the same as those for get_status()
uint8_t set_position(uint8_t stage_i, int32_t position, bool wait_reply = false) {
  // Ensure we start with clear comms channels
  clear_buffers();
  // Sent the move request
  snprintf(linebuffer, sizeof(linebuffer), "%1Xma%08lX", stage_i, (uint32_t)position);
  Serial1.print(linebuffer);
  if (wait_reply) {
    // Valid responses are either status (GS) or position (PO)
    return read_status_response(stage_i);
  }
  return STATUS_OK;
}


// Send movement commands to stages if needed, else query position and update
void do_stage_comms() {
  // Loop through each stage
  for (uint8_t stage_i = 0; stage_i < stage_count; stage_i++) {
    // Check if a movement command is needed to be sent
    if (stages[stage_i].move_required) {
      // Send movement command
      set_position(stage_i, stages[stage_i].position);
      // Update flags
      stages[stage_i].move_required = false;
      last_update_time = millis();
      // Refresh display if is the stage currently being controlled
      if (stage_i == stage_current) {
        display_stage_position(stage_current);
      }
    } else if (millis() > last_update_time + DELAY_UPDATE) {
      // It has been long enough since the last move command was sent
      // Request a position update from the stage
      int32_t position = get_position(stage_i);
      if (position == POSITION_ERROR) {
        // Something went wrong...
        snprintf(linebuffer, sizeof(linebuffer), "%-16s", stages[stage_i].name);
        lcd.setCursor(0, 0);
        lcd.print(linebuffer);
        lcd.setCursor(0, 1);
        lcd.print("Query error!    ");
        delay(DELAY_MSG);
      } else {
        // Response OK, update position data/display
        stages[stage_i].position = position;
        if (stage_i == stage_current) {
          display_stage_position(stage_current);
        }
      }
      // Wait after all stages have been queried
      if (stage_i == stage_count - 1) {
        last_update_time = millis();
      }
    }
  }
}


void setup() {

  Serial.begin(9600);
  Serial1.begin(9600);
  Serial1.setTimeout(500);

  // Initialise the IoAbstraction switches library, tell it to use Arduino pins
  switches.initialise(ioUsingArduino(), true);

  // Configure the rotary encoder button press
  switches.addSwitch(encoderPressPin, onSpinwheelClicked);

  // Configure the rotary encoder
  switches.setEncoder(0, new HardwareRotaryEncoder(encoderAPin, encoderBPin, onEncoderChange, HWACCEL_SLOWER, HALF_CYCLE));
  switches.getEncoder()->setUserIntention(CHANGE_VALUE);
  switches.getEncoder()->changePrecision(65535, encoder_prev, true);

  // Configure the I2C LCD
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("OPA Controller");

  // Try to make contact with each stage over the serial link
  for (uint8_t stage_i = 0; stage_i < stage_count; stage_i++) {
    // Display name of stage being initialised
    snprintf(linebuffer, sizeof(linebuffer), "%-16s", stages[stage_i].name);
    lcd.setCursor(0, 0);
    lcd.print(linebuffer);
    lcd.setCursor(0, 1);
    lcd.print("Initialising... ");
    // Get initial position from EEPROM
    EEPROM.get(stage_i*sizeof(stages[stage_i].position), stages[stage_i].position);
    while (true) {
      // Send status request
      uint8_t status_code = get_status(stage_i);
      if (status_code == STATUS_OK) {
        // Status OK
        lcd.setCursor(0, 1);
        lcd.print("Homing...       ");
        // Home the stage
        home(stage_i);
        // Trigger move to initial/saved position
        stages[stage_i].move_required = true;
        // Wait a bit to let homing complete
        delay(DELAY_HOMING);
        break;
      } else if (status_code < 128) { 
        snprintf(linebuffer, sizeof(linebuffer), "Error code: %4hhu", status_code);
        lcd.setCursor(0, 1);
        lcd.print(linebuffer);
      }
      // Status not OK, wait for a bit and retry
      delay(1000);
    }
  }
  clear_buffers();

  // Schedule communications for stage position updates
  taskManager.scheduleFixedRate(DELAY_LOOP, []{do_stage_comms();}, TIME_MILLIS);
}

void loop() {
  // Perform task manager tasks
  taskManager.runLoop();
}